$(document).ready(function() {
	// Main menu accordion
	
	$('#banner .sliders').each(function() {
		$('#banner .sliders').cycle({ 
			fx:      'fade', 
			speed:    2000, 
			timeout:  3400 
		});
	});
	$( "#menu-accordion" ).accordion({ 
		collapsible: true,
		autoHeight: false, 
	    active: false,
		navigation:true
	});
	
	// Select all checkbox for a set of checkboxes
	$(".select-all").click(function() {
		var checkedStatus = this.checked;
		var checkboxName = this.value;
		$('input[name="' + checkboxName + '[]"]').each(function() {
			this.checked = checkedStatus;
			if(checkedStatus)
			{
				$(this).parent().addClass('checked');
			}
			else
			{
				$(this).parent().removeClass('checked');
			}
		});
	});
	
	$('select.select-submit').change(function() {
		if($(this).val() !== '')
		{
			$(this).closest("form")[0].submit();
		}
	});
	
	tinyMCE.init({
			// General options
			mode : "textareas",
			theme : "advanced",
			editor_selector : "tiny-mce",
			relative_urls : true,
			remove_script_host : false,
			convert_urls : false,
			accessibility_warnings : false,
			plugins : "advimage,advlink,autolink,lists,media,table,jbimages",
			// Theme options
			theme_advanced_buttons1 : "formatselect,fontselect,fontsizeselect,forecolor,backcolor,|,bold,italic,underline,strikethrough",
			theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,outdent,indent,blockquote,|,sub,sup",
			theme_advanced_buttons3 : "link,unlink,|,jbimages,media,charmap,|,tablecontrols,|,hr,removeformat,|,code",
			theme_advanced_buttons4 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : false,
			width: '100%',
			height: '400px'
	});
	
	$("input.date").each(function(index) {
		var name = $(this).attr('name');
		var id = $(this).attr('id');
		var val = $(this).val();
		if(id == null || id == '')
		{
			id = name;
			$(this).attr('id', id);
		}
		
		$(this).parent().append('<input class="datepicker" type="text" id="' + name + '_datepicker" readonly="readonly" />');
		$('#' + name + '_datepicker').datepicker({
			buttonImage : BASE_URL + 'templates/mythos/images/jquery-ui/ui-calendar.png',
			buttonImageOnly : true,
		    showOn : 'both',
			dateFormat : 'yy-mm-dd',
			altFormat : 'yy-mm-dd',
			altField : '#' + id,
			autoSize : true,
			buttonText : '',
			defaultDate : val
		});
		
		$('#' + name + '_datepicker').datepicker('setDate', val);
		$('#' + name + '_datepicker').datepicker('option', 'dateFormat', 'DD, d M yy');
		$('#' + name + '_datepicker').datepicker('option', 'changeYear', true);
		$('#' + name + '_datepicker').datepicker('option', 'changeMonth', true);
		$('#' + name + '_datepicker').datepicker('option', 'showOtherMonths', true);
		$('#' + name + '_datepicker').datepicker('option', 'selectOtherMonths', true);
	});
});
