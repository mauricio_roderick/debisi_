<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>DEBISI | <?php echo template('title') ?></title>

<?php echo template('mythos'); ?>
<?php echo template('bootstrap'); ?>
<?php echo template('head'); ?>
<link rel="stylesheet" href="<?php echo res_url('site/css/style.css') ?>" />


<!--[if lt IE 9]>
<script src="dist/html5shiv.js"></script>
<![endif]-->
</head>
<body>

<div class="container">
	<div class="row header">
		<span class="span3">
			<div class="block smi">
				<div class="label_">CONNECT WITH US</div>

				<div class="icons">
					<a href="" class="fb" >&nbsp;</a>
					<a href="" class="pin" >&nbsp;</a>
					<a href="" class="tw" >&nbsp;</a>
					<a href="" class="insta" >&nbsp;</a>
				</div>
			</div>
		</span>

		<span class="span6">
			<div class="logo">
				<img src="<?php echo res_url('site/images/logo.png') ?>" alt="logo" />
			</div>
		</span>

		<span class="span3">
			<div class="block cart">
				<div class="label_">
					<img class="bag" src="<?php echo res_url('site/images/shoppingBag.png') ?>" /> 
					SHOPPING CART
					<img class="arrow" src="<?php echo res_url('site/images/arrow1.png') ?>" />
					<div class="clr_both"></div>
				</div>
				<table>
					<tr>
						<td>$0</td>
						<td align="right"> / 0 item(s)</td>
					</tr>
				</table>

			</div>
		</span>
	</div>
</div>
<div class="main_menu">
	<div class="container">
		<div class="item_cont">
			<div class="item"><a href="<?php echo site_url() ?>">HOME</a></div>
			<div class="item"><a href="<?php echo site_url('page/index/about-us') ?>">ABOUT US</a></div>
			<div class="item"><a href="<?php echo site_url('product/lists') ?>">SHOP</a></div>
			<div class="item"><a href="<?php echo site_url() ?>">BLOG</a></div>
			<div class="item"><a href="<?php echo site_url() ?>">CONTACT US</a></div>
			<div class="clr_both"></div>
		</div>
		<form>
			<a href="<?php echo site_url() ?>">MyDeBisi</a>
			<a href="<?php echo site_url() ?>">REGISTER</a>
			<div class="fields">
				<input type="text" />
				<input class="search_btn" type="submit" value="" />
			</div>
		</form>
	</div>
</div>

<div class="container">
	<div class="main_content">
		<?php echo template('content') ?>
	</div>

	<div class="footer">
		<div class="top">
			<form class="first division" method="POST">
				<label>JOIN OUR MAILING LIST</label>
				<input type="text" name="" class="field" placeholder="Enter email here" />
				<input type="submit" value="JOIN" class="submit" />
			</form>
			<div class="second division">
				<label class="heading">Quick Links</label>
				<ul>
					<li><a href="<?php echo site_url() ?>" >Gift Cards</a></li>
					<li><a href="<?php echo site_url() ?>" >Gift Card Balance</a></li>
					<li><a href="<?php echo site_url() ?>" >Search Wantlists</a></li>
					<li><a href="<?php echo site_url('page/index/faqs') ?>" >FAQs</a></li>
				</ul>
			</div>
			<div class="third division">
				<label class="heading">Quick Links</label>
				<ul>
					<li><a href="<?php echo site_url() ?>" >Contact Us</a></li>
					<li><a href="<?php echo site_url() ?>" >Order Status</a></li>
					<li><a href="<?php echo site_url('page/index/returnexchange-policy') ?>" >Returns & Exchange Policy</a></li>
					<li><a href="<?php echo site_url() ?>" >Shipping Info</a></li>
					<li><a href="<?php echo site_url() ?>" >Sale</a></li>
				</ul>
			</div>
			<div class="third division">
				<label class="heading">Company</label>
				<ul>
					<li><a href="<?php echo site_url('page/index/about-us') ?>" >About Us</a></li>
					<li><a href="<?php echo site_url() ?>" >Careers</a></li>
					<li><a href="<?php echo site_url() ?>" >Affiliates</a></li>
					<li><a href="<?php echo site_url() ?>" >Press & More</a></li>
				</ul>
			</div>
			<div class="smi division">
				<label class="heading">CONNECT WITH US</label>
				<a href="" class="fb" >&nbsp;</a>
				<a href="" class="pin" >&nbsp;</a>
				<a href="" class="tw" >&nbsp;</a>
				<a href="" class="insta" >&nbsp;</a>
			</div>
			<div class="clr_both"></div>
		</div>
		<div class="bottom">
			<div class="copy"><?php echo '&copy;'.date('Y')." DEBISI's SHOP" ?></div>
			<img src="<?php echo res_url('site/images/payment.jpg') ?>" />
			<div class="clr_both"></div>
		</div>
	</div>



</div><!-- end of container -->



</body>
</html>

<?php /*
<div class="row">
<span class="span12">

<div class="sliderContainer">

<div class="sliderActiveImageContainer">
<img src="<?php echo res_url('site/images/sliderLeftArrow.png') ?>" class="leftArrow" alt="" />

<div class="sliderActiveImage">
<img src="<?php echo res_url('site/images/sliderImg.png') ?>" alt="" />
</div>

<img src="<?php echo res_url('site/images/sliderRightArrow.png') ?>" class="rightArrow" alt="" />
</div>

<div class="sliderSelect">
<div class="sliderSelectText">
<span class="heading">
Lorem ipsum dolor sit amet, consectetur adipisic
</span> <br/>
Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

</div>

<ul class="sliderSelectImage">
<li><img src="<?php echo res_url('site/images/sliderImg1.png') ?>" alt="sliderimg" /></li>
<li><img src="<?php echo res_url('site/images/sliderImg2.png') ?>" alt="sliderimg" /></li>
<li><img src="<?php echo res_url('site/images/sliderImg3.png') ?>" alt="sliderimg" /></li>
<li><img src="<?php echo res_url('site/images/sliderImg4.png') ?>" alt="sliderimg" /></li>
</ul>

</div>

</div><!-- end of slider container -->

</span>
</div> */ ?>
