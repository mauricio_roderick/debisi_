<div class="products">
	<div class="heading">BROWSE PRODUCTS</div>
	
	<div class="list">
		<?php for($x = 0; $x < 9; $x++){ 
		$desc = 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt consectetur adipisicing elit, sed do eiusmod tempor incididunt';
		$title = 'Lorem ipsum doler sit amet Consectetur adipisicing elit, sed do eiusmod tempor incididunt';
		
		?>
		<div class="item">
			<div class="img_cont">
				<div class="blur"></div>
				<img src="<?php echo res_url('site/images/prd2.jpg') ?>" alt="featured" />
				<a href="<?php echo site_url('product/view') ?>">View Details</a>
			</div>
			<div class="desc">
				<div class="title"><?php echo character_limiter($title, 25) ?></div>
				<?php echo character_limiter($desc, 60) ?>
			</div>
		</div>
		<?php } ?>
		<div class="clr_both"></div>
	</div>
</div>