<div class="product_view">
	<div class="left">
		<div class="images">
			<div class="main">
				<img src="<?php echo res_url('site/images/test_prd.jpg') ?>" />
			</div>
			
			<div class="thumbs">
				<div class="cont"><img src="<?php echo res_url('site/images/thumb1.jpg') ?>" /></div>
				<div class="cont"><img src="<?php echo res_url('site/images/thumb2.jpg') ?>" /></div>
				<div class="cont"><img src="<?php echo res_url('site/images/thumb2.jpg') ?>" /></div>
			</div>
		</div>
	</div>
	<div class="right">
		<div class="name">Product Name</div>
		<div class="price">Php 000</div>
		
		<form method="POST" action="">
			<input type="text" class="quantity" value="1" />
			<input type="submit" value="add to cart" class="add_btn" />
			<div>Enter quantity then press button</div>
		</form>
		
		<div class="description">
		<label>Product Description</label>
		Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</div>
	</div>
	
	<div class="clr_both related_products">
		<div class="heading">RELATED PRODUCTS</div>
		<?php for($x = 0; $x < 3; $x++){ 
		$desc = 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt consectetur adipisicing elit, sed do eiusmod tempor incididunt';
		$title = 'Lorem ipsum doler sit amet Consectetur adipisicing elit, sed do eiusmod tempor incididunt';
		
		?>
		<div class="item">
			<div class="img_cont">
				<div class="blur"></div>
				<img src="<?php echo res_url('site/images/prd2.jpg') ?>" alt="featured" />
				<a href="<?php echo site_url() ?>">View Details</a>
			</div>
			<div class="desc">
				<div class="title"><?php echo character_limiter($title, 25) ?></div>
				<?php echo character_limiter($desc, 60) ?>
			</div>
		</div>
		<?php } ?>
		<div class="clr_both"></div>
	</div>
	
	
</div>