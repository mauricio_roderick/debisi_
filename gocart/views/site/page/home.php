<img src="<?php echo res_url('site/images/banner1.jpg') ?>" />
		<div class="featured_products">
			<div class="heading">
				FEATURED PRODUCTS 
			</div>

			<div class="slider">
				<div class="right nav_cont">
					<img src="<?php echo res_url('site/images/arrow_right.png') ?>" alt="" />
				</div>
				<div class="left nav_cont">
					<img src="<?php echo res_url('site/images/arrow_left.png') ?>" alt="" />
				</div>
				<ul class="list_cont">
					<li>
						<div class="img_cont">
							<div class="blur"></div>
							<img src="<?php echo res_url('site/images/prd1.jpg') ?>" alt="featured" />
							<a href="<?php echo site_url() ?>">View Details</a>
						</div>
						<div class="desc">
							<div class="title">Lorem ipsum doler sit amet</div>
							Consectetur adipisicing elit, sed do eiusmod tempor incididunt
						</div>
					</li> 
					<li>
						<div class="img_cont">
							<div class="blur"></div>
							<img src="<?php echo res_url('site/images/prd2.jpg') ?>" alt="featured" />
							<a href="<?php echo site_url() ?>">View Details</a>
						</div>
						<div class="desc">
							<div class="title">Lorem ipsum doler sit amet</div>
							Consectetur adipisicing elit, sed do eiusmod tempor incididunt
						</div>
					</li> 
					<li>
						<div class="img_cont">
							<div class="blur"></div>
							<img src="<?php echo res_url('site/images/prd3.jpg') ?>" alt="featured" />
							<a href="<?php echo site_url() ?>">View Details</a>
						</div>
						<div class="desc">
							<div class="title">Lorem ipsum doler sit amet</div>
							Consectetur adipisicing elit, sed do eiusmod tempor incididunt
						</div>
					</li> 
				</ul>
				<div class="clr_both"></div>
			</div>
		</div>