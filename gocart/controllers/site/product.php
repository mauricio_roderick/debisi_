<?php

class Product extends Base_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function lists()
	{
		$params = array();
		$this->load->helper('text');
		
		$this->template->title('Debisi');
		$this->template->content('product-list', $params, 'site');
		$this->template->show('site');
	}

	public function view($prd_id = 0)
	{
		$params = array();
		$this->load->helper('text');
		
		$this->template->title('Debisi');
		$this->template->content('product-view', $params, 'site');
		$this->template->show('site');
	}

}