<?php

class Page extends Base_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('page_model');
	}

	function index($slug = 'payment')
	{
		$params = array();
		$params['page']	= $page = $this->page_model->get_page_by_slug($slug);
		
		if(! $params['page'])
		{
			show_404();
		}
		
		$this->template->title($page->title);
		$this->template->content('page-index', $params, 'site');
		$this->template->show('site', 'templates-page_template');
	}

	function home()
	{
		$params = array();
		
		$this->template->title('Debisi');
		$this->template->content('page-home', $params, 'site');
		$this->template->show('site');
		
	}

	function about_us()
	{
		$params = array();
		$params['page']	= $page = $this->page_model->get_page_by_slug('about-us');
		
		if(! $page)
		{
			show_404();
		}
		
		$this->template->title($page->title);
		$this->template->content('page-about_us', $params, 'site');
		$this->template->show('site', 'templates-page_template');
	}

}